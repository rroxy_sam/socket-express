module.exports = function (io) {

     var ss = require('socket.io-stream');
     var Grid = require('gridfs-stream');
     var path = require('path');
     var mongoose = require('mongoose');
     //mongoose.connect('mongodb://socketio:socketio@ds021884.mlab.com:21884/socketio');
     mongoose.connect('mongodb://socketuser:socketuser@ds025583.mlab.com:25583/socketdemo');
     //mongoose.connect('mongodb://127.0.0.1:27017/myApp');
     var conn = mongoose.connection;
     Grid.mongo = mongoose.mongo;

     io.on('connection', function(socket){
          ss(socket).on('sendFile', function(stream, data) {
               var filename = path.basename(data.name);
               var newFilename = new Date().getTime() + "_" + filename;
               var buffer = "";
               var mimeType = data.mimeType;
               var gfs = Grid(conn.db);
               var writestream = gfs.createWriteStream({
                    content_type: mimeType,
                    filename: newFilename
               });

               stream.pipe(writestream);
               writestream.on('close', function (file) {
                    var readStream = gfs.createReadStream({
                         filename: newFilename
                    });
                    readStream.setEncoding('base64');
                    readStream.on("data", function (chunk) {
                         buffer += chunk;
                    });
                    readStream.on("end", function () {
                         io.emit('receiveFile', { mediaType: (mimeType.split('/')[0]), buffer: buffer, mimeType : mimeType});
                    });
               });
          });
     });



}
